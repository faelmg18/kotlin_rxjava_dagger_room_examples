package br.com.rhf.schedule.extensions

import android.content.Context
import android.widget.ImageView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import br.com.rhf.schedule.R
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

fun ImageView.loadImageRounded(resourceDrawable: Int = R.drawable.default_user) {
    Glide.with(this.context)
        .load(resourceDrawable)
        .apply(
            RequestOptions.circleCropTransform().placeholder(getProgressCircle(this.context))
        )
        .into(this)
}

fun ImageView.loadImageRounded(url: String?) {

    if (url.isNullOrEmpty()) {
        loadImageRounded(R.drawable.default_user)
        return
    }

    Glide.with(this.context)
        .load(url)
        .apply(RequestOptions.circleCropTransform().placeholder(getProgressCircle(this.context)))
        .into(this)
}

fun ImageView.loadImageSquadFromResource(resourceDrawable: Int = R.drawable.default_user) {
    Glide.with(this.context)
        .load(resourceDrawable)
        .into(this)
}

fun ImageView.loadImageSquad(url: String?) {

    if (url.isNullOrEmpty()) {
        loadImageSquadFromResource()
        return
    }

    Glide.with(this.context)
        .load(url)
        .apply(RequestOptions().placeholder(getProgressCircle(this.context)))
        .into(this)
}


private fun getProgressCircle(context: Context): CircularProgressDrawable {
    val circularProgressDrawable = CircularProgressDrawable(context)
    circularProgressDrawable.strokeWidth = 5f
    circularProgressDrawable.centerRadius = 30f
    circularProgressDrawable.start()
    return circularProgressDrawable
}