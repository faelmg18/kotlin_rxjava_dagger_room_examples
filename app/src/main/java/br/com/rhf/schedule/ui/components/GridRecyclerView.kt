package br.com.rhf.schedule.ui.components

import android.content.Context
import android.util.AttributeSet
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.GridLayoutManager


class GridRecyclerView : RecyclerView {

    companion object {
        const val DEFAULT_NUMBER_COLUMNS = 2
    }

    constructor(context: Context) : super(context) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle) {
        init(context)
    }

    private fun init(context: Context) {
        layoutManager = GridLayoutManager(context, DEFAULT_NUMBER_COLUMNS)
        addItemDecoration(RecyclerViewMargin(20, DEFAULT_NUMBER_COLUMNS))
    }

}
