package br.com.rhf.schedule.util;

import android.content.Context;
import androidx.core.content.ContextCompat;

public class Utilities {
    public static int getColor(Context context, int color) {

        try {
            return ContextCompat.getColor(context, color);

        } catch (Exception e) {
            return color;
        }
    }
}
