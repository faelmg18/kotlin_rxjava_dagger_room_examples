package br.com.rhf.schedule.executor

import br.com.rhf.domain.PostExecutionThread
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UIThread @Inject constructor() : PostExecutionThread {
    override fun getScheduler(): io.reactivex.Scheduler {
        return AndroidSchedulers.mainThread()
    }
}