package br.com.rhf.schedule.util

import android.text.TextWatcher
import android.widget.EditText

interface MaskInterface {
    fun initialize(editText: EditText): TextWatcher
}