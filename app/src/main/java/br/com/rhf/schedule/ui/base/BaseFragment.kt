package br.com.rhf.schedule.ui.base

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import br.com.rhf.schedule.extensions.showToast
import br.com.rhf.schedule.injection.Injectable
import br.com.rhf.schedule.presentation.Resource
import br.com.rhf.schedule.presentation.resources.ResourceState
import java.io.Serializable
import java.lang.reflect.ParameterizedType
import javax.inject.Inject

abstract class BaseFragment<V : BaseViewModel> : Fragment(), Injectable, Serializable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    protected var mActivity: BaseActivity<*>? = null
    protected lateinit var mViewModel: V

    open abstract fun layoutId(): Int
    open abstract fun onCreatedView()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(false)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(layoutId(), null)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        performDataBinding()
        onCreatedView()
    }

    private fun performDataBinding() {
        this.mViewModel = ViewModelProviders.of(this, viewModelFactory).get(viewModel)
        mViewModel.onViewCreated()
    }

    val viewModel: Class<V>
        get() {
            val type = (javaClass
                .genericSuperclass as ParameterizedType).actualTypeArguments[0]
            return type as Class<V>
        }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        if (context is BaseActivity<*>) {
            this.mActivity = context
        }
    }

    override fun onDetach() {
        super.onDetach()
        this.mActivity = null
    }

    protected open fun <T> onStateChange(resource: Resource<T>) {
        when (resource.state) {
            ResourceState.LOADING -> onLoading()
            ResourceState.ERROR -> onError()
            ResourceState.SUCCESS -> onSuccess()
        }
    }

    open fun onLoading() {
        mActivity?.showToast("LOADING")
    }

    open fun onError() {
        mActivity?.showToast("ERROR")
    }

    open fun onSuccess() {
        mActivity?.showToast("SUCCESS")
    }

    fun <T> gotoNextScreen(aClass: Class<T>, bundle: Bundle, requestCode: Int) {
        var intent = Intent(activity, aClass)
        bundle?.let {
            intent.putExtras(bundle)
        }

        startActivityForResult(intent, requestCode)
    }

    fun <T> gotoNextScreen(aClass: Class<T>, bundle: Bundle) {
        var intent = Intent(activity, aClass)
        bundle?.let {
            intent.putExtras(bundle)
        }

        startActivity(intent)
    }
}
