package br.com.rhf.schedule.ui.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import androidx.annotation.ColorInt;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import br.com.rhf.schedule.R;
import br.com.rhf.schedule.util.Utilities;

import java.util.ArrayList;


public class CustomLinearLayoutRoundedBorder extends LinearLayout {

    Context context;
    GradientDrawable shape;
    private float cornerRadiusLeftBottom;
    private float cornerRadiusLeftTop;
    private float cornerRadiusRightBottom;
    private float cornerRadiusRightTop;
    private float cornerRadius;
    private int currentColor;
    private int borderColor;
    private int borderWidth;
    private int color;

    public CustomLinearLayoutRoundedBorder(Context context) {
        super(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public CustomLinearLayoutRoundedBorder(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setUp(context, attrs);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public CustomLinearLayoutRoundedBorder(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setUp(context, attrs);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void setUp(Context context, @Nullable AttributeSet attrs) {
        this.context = context;
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomLinearLayoutRoundedBorder);
        color = a.getInt(R.styleable.CustomLinearLayoutRoundedBorder_colorCustom, android.R.color.transparent);

        cornerRadius = a.getFloat(R.styleable.CustomLinearLayoutRoundedBorder_cornerRadiusCs, 0.0f);

        cornerRadiusLeftBottom = a.getFloat(R.styleable.CustomLinearLayoutRoundedBorder_cornerRadiusLeftBottom, 0.0f);
        cornerRadiusLeftTop = a.getFloat(R.styleable.CustomLinearLayoutRoundedBorder_cornerRadiusLeftTop, 0.0f);
        cornerRadiusRightBottom = a.getFloat(R.styleable.CustomLinearLayoutRoundedBorder_cornerRadiusRightBottom, 0.0f);
        cornerRadiusRightTop = a.getFloat(R.styleable.CustomLinearLayoutRoundedBorder_cornerRadiusRightTop, 0.0f);
        borderColor = a.getColor(R.styleable.CustomLinearLayoutRoundedBorder_cornerBorderColor, Utilities.getColor(context, android.R.color.transparent));
        borderWidth = a.getInt(R.styleable.CustomLinearLayoutRoundedBorder_cornerBorderWidth, 0);

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void setCorners(int color, float cornerRadius) {
        setCorners(color, cornerRadius, 0, 0, 0, 0, android.R.color.transparent, 0);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void setCorners(int color, float cornerRadius, float cornerRadiusLeftBottom, float cornerRadiusLeftTop, float cornerRadiusRightBottom, float cornerRadiusRightTop, int borderColor, int borderWidth) {
        shape = new GradientDrawable();

        currentColor = Utilities.getColor(getContext(), color);

        if (cornerRadius > 0.0f) {
            changeColor(currentColor, cornerRadius, borderColor, borderWidth);
            return;
        }

        ArrayList<Float> values = new ArrayList<>();
        // esqueda topo
        values.add(cornerRadiusLeftTop);
        values.add(cornerRadiusLeftTop);

        //Direita topo
        values.add(cornerRadiusRightTop);
        values.add(cornerRadiusRightTop);

        //direita baixo
        values.add(cornerRadiusRightBottom);
        values.add(cornerRadiusRightBottom);

        //esquerda baixo
        values.add(cornerRadiusLeftBottom);
        values.add(cornerRadiusLeftBottom);

        float[] arrValues = convertIntegers(values);
        shape.setCornerRadii(arrValues);
        shape.setColor(currentColor);
        shape.setStroke(borderWidth, Utilities.getColor(getContext(), Utilities.getColor(context, borderColor)));

        super.setBackground(shape);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);

        int width = getWidth();

        if (cornerRadius == -1) {
            setCorners(color, width / 2, cornerRadiusLeftBottom, cornerRadiusLeftTop, cornerRadiusRightBottom, cornerRadiusRightTop, borderColor, borderWidth);
            return;
        }

        setCorners(color, cornerRadius, cornerRadiusLeftBottom, cornerRadiusLeftTop, cornerRadiusRightBottom, cornerRadiusRightTop, borderColor, borderWidth);


    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void changeColor(int color, float cornerRadius, int borderColor, int borderWidth) {
        GradientDrawable shape = new GradientDrawable();
        shape.setCornerRadius(cornerRadius);
        shape.setStroke(borderWidth, Utilities.getColor(getContext(), Utilities.getColor(context, borderColor)));
        shape.setColor(Utilities.getColor(getContext(), Utilities.getColor(context, color)));

        super.setBackground(shape);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void changeColor(int color, float cornerRadius) {
        GradientDrawable shape = new GradientDrawable();
        shape.setCornerRadius(cornerRadius);
        shape.setStroke(borderWidth, Utilities.getColor(getContext(), Utilities.getColor(context, borderColor)));
        shape.setColor(Utilities.getColor(getContext(), Utilities.getColor(context, color)));

        super.setBackground(shape);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void changeColor(int color) {
        GradientDrawable shape = new GradientDrawable();
        shape.setCornerRadius(cornerRadius);
        shape.setStroke(borderWidth, Utilities.getColor(getContext(), Utilities.getColor(context, borderColor)));
        shape.setColor(Utilities.getColor(getContext(), Utilities.getColor(context, color)));

        super.setBackground(shape);
    }

    @Override
    public void setBackgroundColor(@ColorInt int color) {

        int newColor = Utilities.getColor(getContext(), color);

        setCorners(newColor, cornerRadius, cornerRadiusLeftBottom, cornerRadiusLeftTop, cornerRadiusRightBottom, cornerRadiusRightTop, borderColor, borderWidth);
    }

    public static float[] convertIntegers(ArrayList<Float> integers) {
        float[] ret = new float[integers.size()];
        for (int i = 0; i < ret.length; i++) {
            ret[i] = integers.get(i).intValue();
        }
        return ret;
    }
}
