package br.com.rhf.schedule.extensions

import android.util.Patterns
import android.view.View.OnFocusChangeListener
import android.widget.EditText
import br.com.rhf.schedule.R
import br.com.rhf.schedule.util.CPFUtil
import br.com.rhf.schedule.util.Mask
import br.com.rhf.schedule.util.PhoneMask
import com.google.android.material.textfield.TextInputLayout

fun EditText.addCpfMask() {
    addTextChangedListener(Mask.mask("###.###.###-##", this))
}

fun TextInputLayout.addPhoneMask() {
    editText?.addTextChangedListener(PhoneMask.mask(editText!!))
}

fun TextInputLayout.addCPFValidator(onValidator: (Boolean) -> Unit) {
    editText?.addCpfMask()
    editText?.onFocusChangeListener = OnFocusChangeListener { v, hasFocus ->
        if (!hasFocus) {
            val isValid = CPFUtil.myValidateCPF(editText?.text.toString())
            onValidator.invoke(isValid)
            if (!isValid) {
                isErrorEnabled = true
                error = context.getString(R.string.cpf_invalid)
            } else {
                error = null
            }
        }
    }
}

fun String?.isValidEmail(): Boolean = if (this != null) {
    isNotEmpty() &&
            Patterns.EMAIL_ADDRESS.matcher(this).matches()
} else {
    false
}

fun TextInputLayout.addEmailValidator(onValidator: (Boolean) -> Unit) {
    editText?.onFocusChangeListener = OnFocusChangeListener { v, hasFocus ->
        if (!hasFocus) {
            val isValid = editText?.text.toString().isValidEmail()
            onValidator.invoke(isValid)
            if (!isValid) {
                isErrorEnabled = true
                error = context.getString(R.string.invalid_email)
            } else {
                error = null
            }
        }
    }
}