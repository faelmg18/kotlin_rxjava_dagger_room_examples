package br.com.rhf.schedule.extensions

import android.net.Uri
import android.os.Build
import android.telephony.PhoneNumberUtils
import java.util.*

fun String.toPhoneNumberFormat(): String? =
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        PhoneNumberUtils.formatNumber(this, Locale.getDefault().country)
    } else {
        //Deprecated method
        PhoneNumberUtils.formatNumber(this)
    }

fun String?.isNullOrEmpty(): Boolean = this == null || isEmpty()

fun String.toUri(): Uri = Uri.parse(this)

