package br.com.rhf.schedule.presentation

import androidx.lifecycle.MutableLiveData
import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable

open class DefaultSingleObserver<T>(private val liveDate: MutableLiveData<Resource<T>>) : SingleObserver<T> {

    override fun onSubscribe(d: Disposable) {
        liveDate.setLoading()
    }

    override fun onSuccess(value: T) {
        liveDate.setSuccess(value)
    }

    override fun onError(e: Throwable) {
        liveDate.setError(e.message)
    }
}
