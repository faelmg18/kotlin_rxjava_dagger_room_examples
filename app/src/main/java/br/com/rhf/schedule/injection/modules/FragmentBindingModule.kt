package br.com.rhf.schedule.injection.modules

import br.com.rhf.schedule.ui.fragments.FavoritesContactsFragment
import br.com.rhf.schedule.ui.fragments.SchedulesFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBindingModule {

    @ContributesAndroidInjector
    internal abstract fun contributeSchedulesFragment(): SchedulesFragment

    @ContributesAndroidInjector
    internal abstract fun contributeFavoritesContactsFragment(): FavoritesContactsFragment

}
