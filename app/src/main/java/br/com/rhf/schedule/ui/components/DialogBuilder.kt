package br.com.rhf.schedule.ui.components

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface

import br.com.rhf.domain.model.Phone
import br.com.rhf.schedule.R
import br.com.rhf.schedule.extensions.inflateLayout
import br.com.rhf.schedule.util.MaskInterface
import com.google.android.material.textfield.TextInputEditText

object DialogBuilder {

    fun showDialogYeOrNo(
        context: Context,
        message: Int,
        positiveClick: (DialogInterface) -> Unit?,
        negativeClick: (DialogInterface) -> Unit?,
        cancelable: Boolean = false,
        title: Int = R.string.atention,
        buttonOkTitle: Int = R.string.ok,
        buttonCancelTitle: Int = R.string.cancel
    ) {
        showDialogYeOrNo(
            context,
            context.getString(message),
            positiveClick,
            negativeClick,
            cancelable,
            context.getString(title),
            context.getString(buttonOkTitle),
            context.getString(buttonCancelTitle)
        )
    }

    fun showDialogYeOrNo(
        context: Context,
        message: String,
        positiveClick: (DialogInterface) -> Unit?,
        negativeClick: (DialogInterface) -> Unit?,
        cancelable: Boolean = false,
        title: String = context.getString(R.string.atention),
        buttonOkTitle: String = context.getString(R.string.ok),
        buttonCancelTitle: String = context.getString(R.string.cancel)
    ) {

        var dialog = AlertDialog.Builder(context)
        dialog.setTitle(title)
        dialog.setMessage(message)

        dialog.setPositiveButton(buttonOkTitle) { dialog, which ->
            positiveClick?.invoke(dialog)
        }

        dialog.setNegativeButton(buttonCancelTitle) { dialog, which ->
            negativeClick?.invoke(dialog)
        }

        (title.isNotEmpty()).let {
            if (it) {
                dialog.setTitle(title)
            }
        }

        dialog.setCancelable(cancelable)
        dialog.show()
    }

    fun showDialogConfirmation(
        context: Context,
        message: Int,
        positiveClick: (DialogInterface) -> Unit?,
        title: Int = R.string.atention,
        cancelable: Boolean = false,
        buttonOkTitle: Int = R.string.ok
    ) {

        var dialog = AlertDialog.Builder(context)

        (title > 0).let {
            if (it) {
                dialog.setTitle(title)
            }
        }

        dialog.setMessage(message)

        dialog.setPositiveButton(buttonOkTitle) { dialog, which ->
            positiveClick?.invoke(dialog)
        }

        dialog.setCancelable(cancelable)
        dialog.show()
    }

    fun showListPhonesDialog(
        title: String,
        phones: ArrayList<Phone>,
        context: Activity,
        onClickOk: ((itemSelected: String) -> Unit)
    ) {

        val result: Array<CharSequence> =
            phones.map { array -> array.phoneText as CharSequence }.toTypedArray()

        showListDialog(title, result, context, onClickOk)
    }

    fun showListPhonesType(
        title: String,
        phones: ArrayList<String>,
        context: Activity,
        onClickOk: ((itemSelected: String) -> Unit)
    ) {

        val result: Array<CharSequence> =
            phones.map { array -> array as CharSequence }.toTypedArray()


        showListDialog(title, result, context, onClickOk)
    }

    fun showListDialog(
        title: String,
        values: Array<CharSequence>,
        context: Activity,
        onClickOk: ((itemSelected: String) -> Unit)
    ) {

        val builder = AlertDialog.Builder(context)
        with(builder)
        {
            setTitle(title)
            setItems(values) { dialog, which ->
                dialog.dismiss()
                onClickOk.invoke(values[which].toString())
            }
        }

        val dialog = builder.create()
        dialog.show()
    }

    fun showCreateDialogWithEditText(
        context: Context,
        message: String,
        itemClick: (String) -> Unit,
        hint: String = "",
        mandatory: Boolean = true,
        mandatoryMessage: Int = R.string.mandatory_filed,
        cancelable: Boolean = true,
        mask: MaskInterface? = null,
        inputType: Int = -1
    ) {
        val builder = AlertDialog.Builder(context)
        builder.setTitle(message)

        val view = context.inflateLayout(R.layout.dialog_new_with_edit_text)

        val editText = view.findViewById(R.id.editText) as TextInputEditText
        editText.hint = hint
        builder.setView(view)
        builder.setCancelable(cancelable)

        if (inputType > -1) {
            editText.inputType = inputType
        }

        if (mask != null) {
            editText.addTextChangedListener(mask.initialize(editText))
        }

        builder.setPositiveButton(R.string.ok) { dialog, p1 ->
            val value = editText.text.toString()
            var isValid = true

            if (mandatory) {

                if (value.isNullOrEmpty()) {
                    editText.error = context.getString(mandatoryMessage)
                    isValid = false
                }

                if (isValid) {
                    itemClick.invoke(value)
                }
            }

            if (isValid) {
                dialog.dismiss()
            }
        }

        builder.setNegativeButton(android.R.string.cancel) { dialog, p1 ->
            dialog.cancel()
        }

        builder.show()
    }
}

