package br.com.rhf.schedule.presentation.resources

sealed class ResourceState {
    object INITIAL : ResourceState()
    object LOADING : ResourceState()
    object SUCCESS : ResourceState()
    object ERROR : ResourceState()
}