package br.com.rhf.schedule.ui.base


import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import br.com.rhf.schedule.extensions.showToast
import br.com.rhf.schedule.presentation.Resource
import br.com.rhf.schedule.presentation.resources.ResourceState
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import java.lang.reflect.ParameterizedType
import javax.inject.Inject

abstract class BaseActivity<V : BaseViewModel> : AppCompatActivity(), HasSupportFragmentInjector {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    protected lateinit var mViewModel: V

    val viewModel: Class<V>
        get() {
            val type = (javaClass
                .genericSuperclass as ParameterizedType).actualTypeArguments[0]
            return type as Class<V>
        }

    protected abstract fun myOnCreate(savedInstanceState: Bundle?)

    protected abstract fun layoutId(): Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val layoutId = layoutId()

        if (layoutId() != 0) {
            setContentView(layoutId)
        }

        performDataBinding()
        myOnCreate(savedInstanceState)
    }

    private fun performDataBinding() {
        this.mViewModel = ViewModelProviders.of(this, viewModelFactory).get(viewModel)
        mViewModel.onViewCreated()
    }

    override fun supportFragmentInjector(): DispatchingAndroidInjector<Fragment> {
        return dispatchingAndroidInjector
    }

    protected open fun <T> onStateChange(resource: Resource<T>) {
        when (resource.state) {
            ResourceState.LOADING -> onLoading()
            ResourceState.ERROR -> onError()
            ResourceState.SUCCESS -> onSuccess()
        }
    }

    fun <T> gotoNextScreen(aClass: Class<T>, bundle: Bundle? = null, finish: Boolean = false) {
        var intent = Intent(this, aClass)
        bundle?.let {
            intent.putExtras(bundle)
        }

        startActivity(intent)

        if (finish) {
            finish()
        }
    }

    fun <T> gotoNextScreen(aClass: Class<T>, bundle: Bundle?, requestCode: Int) {
        var intent = Intent(this, aClass)
        bundle?.let {
            intent.putExtras(bundle)
        }

        startActivityForResult(intent, requestCode)
    }

    open fun onLoading() {
        this.showToast("LOADING")
    }

    open fun onError() {
        this.showToast("ERROR")
    }

    open fun onSuccess() {
        this.showToast("SUCCESS")
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when (item?.itemId) {
            android.R.id.home -> {
                finish()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    protected fun addBackButtonOnToolbar() {
        supportActionBar?.let {
            it.setDisplayHomeAsUpEnabled(true)
            it.setDisplayShowHomeEnabled(true)
        }
    }

    protected fun finishWithResultOk() {
        setResult(Activity.RESULT_OK)
        finish()
    }
}
