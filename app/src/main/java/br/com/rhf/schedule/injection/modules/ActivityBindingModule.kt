package br.com.rhf.schedule.injection.modules

import br.com.rhf.schedule.MainActivity
import br.com.rhf.schedule.ui.activities.contacts.CreateContactActivity
import br.com.rhf.schedule.ui.activities.schedule.ScheduleDetailActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {

    @ContributesAndroidInjector(modules = [FragmentBindingModule::class])
    internal abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector
    internal abstract fun bindScheduleDetailActivity(): ScheduleDetailActivity

    @ContributesAndroidInjector
    internal abstract fun bindCreateContactActivity(): CreateContactActivity
}
