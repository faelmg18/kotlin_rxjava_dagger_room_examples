package br.com.rhf.schedule.ui.activities.contacts

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.Observer
import br.com.rhf.domain.model.Contact
import br.com.rhf.domain.model.Phone
import br.com.rhf.domain.model.Schedule
import br.com.rhf.schedule.MainActivity.Companion.SCHEDULE_ID_KEY
import br.com.rhf.schedule.R
import br.com.rhf.schedule.extensions.*
import br.com.rhf.schedule.presentation.resources.ResourceState
import br.com.rhf.schedule.ui.base.BaseActivity
import br.com.rhf.schedule.util.CPFUtil
import com.github.dhaval2404.imagepicker.ImagePicker
import kotlinx.android.synthetic.main.create_contact_activity.*


class CreateContactActivity : BaseActivity<CreateContactViewModel>() {

    var filePath: String? = ""

    var schedule: Schedule? = null
    override fun layoutId(): Int {
        return R.layout.create_contact_activity
    }

    override fun myOnCreate(savedInstanceState: Bundle?) {

        setTitle(R.string.create_new_contact)

        addBackButtonOnToolbar()

        txtInpCpf.addCPFValidator { }

        txtInpEmail.addEmailValidator { }

        fabAddPhoto.setOnClickListener { this.startImagePicker() }

        txtInpPhone.addPhoneMask()

        bindObserver()

        loadScheduleFromBundle()
    }

    private fun loadScheduleFromBundle() {
        val bundle = intent.extras

        bundle?.let {
            val idSchedule = it.getLong(SCHEDULE_ID_KEY)
            mViewModel.getScheduleById(idSchedule)
        }
    }

    override fun onBackPressed() {
        if (schedule != null) {
            finishWithResultOk()
        } else {
            super.onBackPressed()
        }
    }

    private fun bindObserver() {
        mViewModel.contactMutableLiveData.observe(this, Observer {
            if (it) {
                showToast(getString(R.string.contact_save_with_success))
                finishWithResultOk()
            } else showToast(getString(R.string.contact_not_save_with_success))
        })

        mViewModel.scheduleMutableLiveData.observe(this, Observer {

            onStateChange(it)

            when (it.state) {
                ResourceState.SUCCESS -> {
                    schedule = it.data
                    onSchedule()
                }
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_save, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (resultCode) {
            Activity.RESULT_OK -> {

                val fileUri = data?.data

                imageUser.setImageURI(fileUri)

                filePath = ImagePicker.getFilePath(data)
            }
            ImagePicker.RESULT_ERROR -> this.showToast(ImagePicker.getError(data))

        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        return when (item?.itemId) {
            R.id.save_contact -> {
                createContactAndSave()
                return true
            }

            android.R.id.home -> {
                return if (schedule != null) {
                    finishWithResultOk()
                    true
                } else {
                    super.onOptionsItemSelected(item)
                }
            }
            else -> return false
        }
    }

    private fun createContactAndSave() {

        val name = txtInpName.editText?.text.toString()
        val lastName = txtInpLast.editText?.text.toString()
        val cpf = txtInpCpf.editText?.text.toString()
        val email = txtInpEmail.editText?.text.toString()
        val phone = txtInpPhone.editText?.text.toString()
        val type = spinnerPhoneType.selectedItem.toString()
        val photoUri = filePath

        if (!CPFUtil.myValidateCPF(cpf)) {
            showToast(getString(R.string.cpf_invalid))
            return
        }

        if (!email.isValidEmail()) {
            showToast(getString(R.string.invalid_email))
            return
        }

        if (schedule != null) {
            schedule!!.nome = name
            schedule!!.sobrenome = lastName
            schedule!!.CPF = cpf
            schedule!!.email = email

            if (!photoUri.isNullOrEmpty() && !photoUri.equals(schedule!!.foto)) {
                schedule!!.foto = photoUri
            }

            mViewModel.updateContact(schedule!!)
            return
        }

        val schedule = Schedule(0, name, lastName, cpf, email, photoUri, false)

        val phoneModel = Phone(phone, type)

        val contact = Contact(schedule, arrayListOf(phoneModel))

        mViewModel.saveContact(contact)
    }


    private fun onSchedule() {
        txtInpName.editText?.setText(schedule?.nome)
        txtInpLast.editText?.setText(schedule?.sobrenome)
        txtInpCpf.editText?.setText(schedule?.CPF)
        txtInpEmail.editText?.setText(schedule?.email)
        txtInpPhone.editText?.visibility = View.GONE
        spinnerPhoneType.visibility = View.GONE
        imageUser.setImageURI(schedule?.foto?.toUri())
    }
}