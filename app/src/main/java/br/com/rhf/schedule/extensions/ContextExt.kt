package br.com.rhf.schedule.extensions

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

fun Context.inflateLayout(layoutId: Int, rootViewGroup: ViewGroup? = null, attachRoot: Boolean = false): View =
    LayoutInflater.from(this).inflate(layoutId, rootViewGroup, attachRoot)
