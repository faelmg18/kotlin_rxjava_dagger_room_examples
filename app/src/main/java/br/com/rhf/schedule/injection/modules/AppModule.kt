package br.com.rhf.schedule.injection.modules

import android.app.Application
import android.content.Context
import br.com.rhf.data.executor.JobExecutor
import br.com.rhf.domain.PostExecutionThread
import br.com.rhf.domain.executor.ThreadExecutor
import br.com.rhf.schedule.executor.UIThread
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {
    @Provides
    @Singleton
    internal fun provideContext(application: Application): Context {
        return application
    }

    @Provides
    @Singleton
    internal fun provideThreadExecutor(jobExecutor: JobExecutor): ThreadExecutor {
        return jobExecutor
    }

    @Provides
    @Singleton
    internal fun providePostExecutionThread(uiThread: UIThread): PostExecutionThread {
        return uiThread
    }
}