package br.com.rhf.schedule

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import br.com.rhf.schedule.ui.activities.contacts.CreateContactActivity
import br.com.rhf.schedule.ui.activities.home.MainViewModel
import br.com.rhf.schedule.ui.base.BottomBarActivity

class MainActivity : BottomBarActivity<MainViewModel>() {

    companion object {
        const val SCHEDULE_KEY = "Schedule"
        const val SCHEDULE_ID_KEY = "ScheduleId"
        const val UPDATE_SCHEDULES_REQUEST_CODE: Int = 2233
    }

    override fun layoutId(): Int {
        return R.layout.activity_main
    }

    override fun myOnCreate(savedInstanceState: Bundle?) {
        super.myOnCreate(savedInstanceState)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        val menuItem = menu.findItem(R.id.action_add)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        currentFragment?.onActivityResult(requestCode, resultCode, data)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when (item?.itemId) {
            R.id.action_add -> {
                gotoNextScreen(CreateContactActivity::class.java, null, UPDATE_SCHEDULES_REQUEST_CODE)
            }
        }

        return super.onOptionsItemSelected(item)
    }

}
