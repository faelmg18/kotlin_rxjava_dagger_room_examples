package br.com.rhf.schedule.presentation

import br.com.rhf.schedule.presentation.resources.ResourceState

data class Resource<out T> constructor(
    val state: ResourceState,
    val data: T? = null,
    val message: String? = null
)