package br.com.rhf.schedule.ui.activities.schedule

import android.app.Activity
import android.os.Bundle
import android.text.InputType
import android.view.Menu
import android.view.MenuItem
import androidx.lifecycle.Observer
import br.com.rhf.domain.model.Phone
import br.com.rhf.domain.model.Schedule
import br.com.rhf.schedule.MainActivity.Companion.SCHEDULE_ID_KEY
import br.com.rhf.schedule.R
import br.com.rhf.schedule.extensions.loadImageSquad
import br.com.rhf.schedule.extensions.showToast
import br.com.rhf.schedule.ui.activities.contacts.CreateContactActivity
import br.com.rhf.schedule.ui.adapters.PhoneListAdapter
import br.com.rhf.schedule.ui.base.BaseActivity
import br.com.rhf.schedule.ui.components.DialogBuilder
import br.com.rhf.schedule.util.PhoneMask
import kotlinx.android.synthetic.main.schedule_detail.*


class ScheduleDetailActivity : BaseActivity<ScheduleDetailViewModel>() {

    companion object {
        const val REQUEST_PHONE_CALL = 1
    }

    lateinit var schedule: Schedule

    private val itemDeletePhoneClick: (Phone) -> Unit =
        { phone ->
            DialogBuilder.showDialogYeOrNo(this, getString(R.string.remove_phone), {
                mViewModel.deletePhone(phone)
            }, {})
        }

    private lateinit var adapter: PhoneListAdapter

    var scheduleId: Long = 0

    override fun layoutId(): Int {
        return R.layout.schedule_detail
    }

    override fun myOnCreate(savedInstanceState: Bundle?) {
        addBackButtonOnToolbar()
        bindObserver()
        loadElement()
    }

    private fun loadElement() {
        var bundle = intent.extras

        bundle?.let {
            scheduleId = bundle.getLong(SCHEDULE_ID_KEY)
        }

        adapter = PhoneListAdapter(itemDeletePhoneClick, this)
        phoneRecyclerView.adapter = adapter
    }

    override fun onResume() {
        super.onResume()

        if (scheduleId > 0) {
            mViewModel.getSchedule(scheduleId)
        }
    }

    private fun onSchedule() {

        textViewName.text = schedule.nome

        imageProfile.loadImageSquad(schedule.foto)

        if (!schedule.favorite) {
            setNoFavorite()
        }
        favoriteImage.setOnClickListener {
            changeFavorite(!schedule.favorite)
        }

        deleteContact.setOnClickListener {
            DialogBuilder.showDialogYeOrNo(this@ScheduleDetailActivity, R.string.delete_contact, {
                mViewModel.deleteSchedule(schedule)
                it.dismiss()
            }, {
                it.dismiss()
            })
        }
        addPhone.setOnClickListener {
            showDialogAddPhone()
        }

        getPhones()
    }

    private fun showDialogAddPhone() {
        DialogBuilder.showCreateDialogWithEditText(
            this, getString(R.string.entrer_phone_number), {
                showDialogChoosePhoneType(it)

            }, getString(R.string.phone),
            true, R.string.mandatory_filed, true, PhoneMask::class.java.newInstance(),
            InputType.TYPE_CLASS_NUMBER
        )
    }

    private fun showDialogChoosePhoneType(phone: String) {

        val arrayPhoneType = resources.getStringArray(R.array.phone_types_arrays)

        val result: Array<CharSequence> =
            arrayPhoneType.map { array -> array as CharSequence }.toTypedArray()

        DialogBuilder.showListDialog(getString(R.string.choose_phone_type_label), result, this) {
            val phone = Phone(phone, it, 0, schedule.id)
            mViewModel.insertPhone(phone)
        }
    }

    private fun getPhones() {
        mViewModel.finAllPhoneByScheduleId(schedule)
    }

    private fun bindObserver() {
        mViewModel.scheduleUpdateMutableLiveData.observe(this, Observer {
            if (it) {
                this.showToast(getString(R.string.data_updated_with_success))
                return@Observer
            }
            changeFavorite(!schedule.favorite)
        })

        mViewModel.scheduleDeleteMutableLiveData.observe(this, Observer {
            if (it) {
                DialogBuilder.showDialogConfirmation(this@ScheduleDetailActivity, R.string.delete_with_success, {
                    setResult(Activity.RESULT_OK)
                    finish()
                })
            }
        })

        mViewModel.scheduleMutableLiveData.observe(this, Observer {
            it.data?.let {
                this.schedule = it
                onSchedule()
            }
        })

        mViewModel.phonesMutableLiveData.observe(this, Observer {
            adapter.submitList(it.data)
        })

        mViewModel.phoneDeleteMutableLiveData.observe(this, Observer { delete ->
            run {
                if (delete) {
                    showToast(getString(R.string.delete_with_success))
                    getPhones()

                } else {
                    showToast(getString(R.string.phone_not_deleted))
                }
            }
        })

        mViewModel.phoneInsertedMutableLiveData.observe(this, Observer { saved ->
            if (saved) {
                showToast(getString(R.string.phone_saved_with_success))
                getPhones()

            } else {
                showToast(getString(R.string.phone_not_saved_with_success))
            }
        })
    }

    private fun changeFavorite(favorite: Boolean) {
        schedule.favorite = favorite
        mViewModel.updateSchedule(schedule)
        if (favorite) {
            setFavorite()
        } else {
            setNoFavorite()
        }
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_edit, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when (item?.itemId) {
            R.id.edit_contact -> {
                gotoCreateContactModeEdit()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun gotoCreateContactModeEdit() {
        val bundle = Bundle()
        bundle.putLong(SCHEDULE_ID_KEY, schedule.id)
        gotoNextScreen(CreateContactActivity::class.java, bundle)
    }

    override fun onBackPressed() {
        finish()
    }

    private fun setFavorite() {
        changeIconImageFavorite(R.drawable.ic_favorite)
    }

    private fun setNoFavorite() {
        changeIconImageFavorite(R.drawable.ic_favorite_border)
    }

    private fun changeIconImageFavorite(resource: Int) {
        favoriteImage.setImageResource(resource)
    }

}