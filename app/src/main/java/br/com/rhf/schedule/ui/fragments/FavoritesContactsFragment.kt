package br.com.rhf.schedule.ui.fragments

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import br.com.rhf.domain.model.Phone
import br.com.rhf.domain.model.Schedule
import br.com.rhf.schedule.MainActivity.Companion.SCHEDULE_ID_KEY
import br.com.rhf.schedule.MainActivity.Companion.UPDATE_SCHEDULES_REQUEST_CODE
import br.com.rhf.schedule.R
import br.com.rhf.schedule.extensions.*
import br.com.rhf.schedule.presentation.Resource
import br.com.rhf.schedule.presentation.resources.ResourceState
import br.com.rhf.schedule.ui.activities.schedule.ScheduleDetailActivity
import br.com.rhf.schedule.ui.adapters.FavoritesAdapter
import br.com.rhf.schedule.ui.base.BaseFragment
import br.com.rhf.schedule.ui.components.DialogBuilder
import br.com.rhf.schedule.util.PhoneMask
import kotlinx.android.synthetic.main.list_schedules.*

class FavoritesContactsFragment : BaseFragment<SchedulesViewModel>() {

    private val itemClick: (Schedule) -> Unit =
        {

            var bundle = Bundle()
            bundle.putLong(SCHEDULE_ID_KEY, it.id)
            gotoNextScreen(ScheduleDetailActivity::class.java, bundle, UPDATE_SCHEDULES_REQUEST_CODE)
        }

    private val callClick: (Schedule) -> Unit =
        {
            mViewModel.finAllPhoneByScheduleId(it)
        }
    private lateinit var adapter: FavoritesAdapter

    override fun layoutId(): Int {
        return R.layout.favorites_contacts_fragment
    }

    override fun onCreatedView() {

        adapter = FavoritesAdapter(itemClick, callClick, activity)
        scheduleRecyclerView.adapter = adapter
        bindObservers()
        swipeRefreshLayout.setOnRefreshListener { getSchedules() }
        getSchedules()
    }

    fun bindObservers() {
        mViewModel.phonesMutableLiveData.observe(this, Observer { it ->

            when (it.state) {
                ResourceState.SUCCESS -> {
                    val list = arrayListOf<Phone>()
                    it.data?.let { it ->
                        it.forEach {
                            list.add(it)
                        }
                    }
                    activity?.let { it1 ->
                        DialogBuilder.showListPhonesDialog(getString(R.string.select_phone_number), list, it1) {
                            it1.checkPhoneCallPermissionOrStartCallPhone(PhoneMask.replaceChars(it))
                        }
                    }
                }
            }
        })

        mViewModel.scheduleMutableLiveData.observe(this, Observer { updateView(it) })
    }

    private fun getSchedules() {
        mViewModel.getIScheduleFavorites()
    }

    private fun updateView(resource: Resource<MutableList<Schedule>>?) {
        resource?.let {

            onStateChange(it)

            it.data.let { schedules ->
                adapter.submitList(it.data?.sortedBy { it.nome })
            }
            it.message?.let {
                mActivity?.showToast("error $it")
            }
        }
    }

    override fun onSuccess() {
        swipeRefreshLayout.stopRefreshing()
    }

    override fun onError() {
        swipeRefreshLayout.stopRefreshing()
    }

    override fun onLoading() {
        swipeRefreshLayout.startRefreshing()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                UPDATE_SCHEDULES_REQUEST_CODE -> {
                    getSchedules()
                }
            }
        }
    }
}