package br.com.rhf.schedule.extensions

import android.Manifest
import android.R
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import br.com.rhf.schedule.ui.activities.schedule.ScheduleDetailActivity
import br.com.rhf.schedule.ui.components.DialogBuilder
import com.github.dhaval2404.imagepicker.ImagePicker


const val IMAGE_PICKER_REQUEST = 1452

fun Activity.showToast(message: String, duration: Int = Toast.LENGTH_LONG) {
    Toast.makeText(this, message, duration).show()
}

fun Context.getMColor(context: Context = this, resourceId: Int): Int {
    return return try {
        ContextCompat.getColor(context, resourceId)
    } catch (ex: Exception) {
        ContextCompat.getColor(context, R.color.black)
    }
}

private fun Activity.startPhoneCallIntent(phoneNumber: String) {
    val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel: $phoneNumber"))
    startActivity(intent)
}

fun Activity.checkPhoneCallPermissionOrStartCallPhone(phoneText: String?) {
    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.CALL_PHONE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.CALL_PHONE),
                ScheduleDetailActivity.REQUEST_PHONE_CALL
            )
        } else {
            phoneText?.let {
                this.startPhoneCallIntent(it)
            }

        }
    } else {
        phoneText?.let {
            this.startPhoneCallIntent(it)
        }
    }
}

fun Activity.startImagePicker() {

    DialogBuilder.showDialogYeOrNo(this, "",
        {
            showImagePickerOnlyGallery(this)
        }, {
            showImagePickerOnlyCamera(this)
        }, true, "Escolha", "Galeria", "Camera"
    )
}

private fun showImagePickerOnlyGallery(activity: Activity) {
    ImagePicker.with(activity)
        .crop(1f, 1f)                //Crop Square image(Optional)
        .compress(1024)            //Final image size will be less than 1 MB(Optional)
        .maxResultSize(1080, 1080)    //Final image resolution will be less than 1080 x 1080(Optional)
        .galleryOnly()
        .start(IMAGE_PICKER_REQUEST)
}

private fun showImagePickerOnlyCamera(activity: Activity) {
    ImagePicker.with(activity)
        .crop(1f, 1f)                //Crop Square image(Optional)
        .compress(1024)            //Final image size will be less than 1 MB(Optional)
        .maxResultSize(1080, 1080)    //Final image resolution will be less than 1080 x 1080(Optional)
        .cameraOnly()
        .start(IMAGE_PICKER_REQUEST)
}