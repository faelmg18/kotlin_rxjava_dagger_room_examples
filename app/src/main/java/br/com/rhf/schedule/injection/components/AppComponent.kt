package br.com.rhf.schedule.injection.components

import android.app.Application
import br.com.rhf.data.database.ScheduleDatabase
import br.com.rhf.data.database.data.ScheduleDao
import br.com.rhf.data.database.repositories.ScheduleRepository
import br.com.rhf.data.modules.*
import br.com.rhf.schedule.AndroidApplication
import br.com.rhf.schedule.injection.modules.ActivityBindingModule
import br.com.rhf.schedule.injection.modules.AppModule
import br.com.rhf.schedule.injection.modules.RepositoryModule
import br.com.rhf.schedule.injection.modules.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        AppModule::class,
        ActivityBindingModule::class,
        FactoryModule::class,
        UrlApiModule::class,
        RequestModule::class,
        NetworkModule::class,
        ViewModelModule::class,
        RepositoryModule::class,
        InterceptorModule::class,
        SecurityModule::class,
        RoomModule::class
    ]
)

interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun api(urlApiModule: UrlApiModule): Builder

        fun roomModule(roomModule: RoomModule): Builder

        fun build(): AppComponent
    }

    fun inject(androidApplication: AndroidApplication)

    fun scheduleDao(): ScheduleDao

    fun scheduleDatabase(): ScheduleDatabase

    fun scheduleRepository(): ScheduleRepository

    fun application(): Application

}