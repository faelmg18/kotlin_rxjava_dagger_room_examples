package br.com.rhf.schedule.util

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import br.com.rhf.schedule.extensions.toPhoneNumberFormat

class PhoneMask : MaskInterface {

    override fun initialize(editText: EditText): TextWatcher {
        return mask(editText)
    }

    companion object {
        fun replaceChars(phoneFull: String?): String? {
            return phoneFull?.replace(".", "")?.replace("-", "")
                ?.replace("(", "")?.replace(")", "")
                ?.replace("/", "")?.replace(" ", "")
                ?.replace("*", "")
        }

        fun mask(et: EditText): TextWatcher {

            return object : TextWatcher {

                override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    et.removeTextChangedListener(this)

                    val str = replaceChars(s.toString())
                    var telWithMask: String? = ""

                    telWithMask = str?.toPhoneNumberFormat()

                    telWithMask?.let {
                        et.setText(telWithMask)
                        et.setSelection(telWithMask.length)
                    }

                    et.addTextChangedListener(this)

                }

                override fun afterTextChanged(editable: Editable) {

                }
            }
        }
    }
}