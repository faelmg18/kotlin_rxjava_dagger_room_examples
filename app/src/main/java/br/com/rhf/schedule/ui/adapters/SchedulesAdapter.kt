package br.com.rhf.schedule.ui.adapters

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import br.com.rhf.domain.model.Schedule
import br.com.rhf.schedule.R
import br.com.rhf.schedule.extensions.inflate
import kotlinx.android.synthetic.main.item_list_schedule.view.*

class SchedulesAdapter constructor(private val itemClick: (Schedule) -> Unit) :
    ListAdapter<Schedule, SchedulesAdapter.ViewHolder>(ScheduleDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(parent)

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(getItem(position))

    inner class ViewHolder(parent: ViewGroup) :
        RecyclerView.ViewHolder(parent.inflate(R.layout.item_list_schedule)) {

        fun bind(item: Schedule) {
            itemView.textViewName.text = item.nome
            itemView.textViewInitialName.text = item.nome?.first().toString().toUpperCase()
            itemView.setOnClickListener { itemClick.invoke(item) }
        }
    }

    private class ScheduleDiffCallback : DiffUtil.ItemCallback<Schedule>() {
        override fun areItemsTheSame(oldItem: Schedule, newItem: Schedule): Boolean =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: Schedule, newItem: Schedule): Boolean =
            oldItem == newItem
    }
}
