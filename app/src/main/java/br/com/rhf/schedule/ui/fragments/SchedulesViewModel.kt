package br.com.rhf.schedule.ui.fragments

import androidx.lifecycle.MutableLiveData
import br.com.rhf.domain.interector.GetFavoritesSchedules
import br.com.rhf.domain.interector.GetPhones
import br.com.rhf.domain.interector.GetSchedules
import br.com.rhf.domain.model.Phone
import br.com.rhf.domain.model.Schedule
import br.com.rhf.schedule.presentation.DefaultSingleObserver
import br.com.rhf.schedule.presentation.Resource
import br.com.rhf.schedule.ui.base.BaseViewModel
import javax.inject.Inject

class SchedulesViewModel @Inject constructor(
    private var getSchedules: GetSchedules,
    private val getPhones: GetPhones,
    private var getFavoritesSchedules: GetFavoritesSchedules
) : BaseViewModel() {

    val scheduleMutableLiveData = MutableLiveData<Resource<MutableList<Schedule>>>()
    val phonesMutableLiveData = MutableLiveData<Resource<MutableList<Phone>>>()

    fun getISchedule() {
        getSchedules.executeSingle(
            object : DefaultSingleObserver<MutableList<Schedule>>(scheduleMutableLiveData) {},
            null
        )
    }

    override fun myOnCleared() {
        getSchedules.dispose()
    }

    fun getIScheduleFavorites() {
        getFavoritesSchedules.executeSingle(object :
            DefaultSingleObserver<MutableList<Schedule>>(scheduleMutableLiveData) {}, null)
    }

    fun finAllPhoneByScheduleId(schedule: Schedule) {
        getPhones.executeSingle(object : DefaultSingleObserver<MutableList<Phone>>(phonesMutableLiveData) {}, schedule)
    }
}