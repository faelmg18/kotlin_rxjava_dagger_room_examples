package br.com.rhf.schedule.injection.modules

import br.com.rhf.data.remote.repository.ScheduleImpl
import br.com.rhf.domain.repository.ISchendules
import dagger.Module
import dagger.Provides

import javax.inject.Singleton

@Module
class RepositoryModule {

    @Provides
    @Singleton
    internal fun provideSchedulerImpl(scheduleImpl: ScheduleImpl): ISchendules {
        return scheduleImpl
    }
}
