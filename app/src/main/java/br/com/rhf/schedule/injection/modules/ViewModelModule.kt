package br.com.rhf.schedule.injection.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import br.com.rhf.schedule.injection.ViewModelKey
import br.com.rhf.schedule.ui.activities.contacts.CreateContactViewModel
import br.com.rhf.schedule.ui.activities.home.MainViewModel
import br.com.rhf.schedule.ui.activities.schedule.ScheduleDetailViewModel
import br.com.rhf.schedule.ui.common.ViewModelProviderFactory
import br.com.rhf.schedule.ui.fragments.SchedulesViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelProviderFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    internal abstract fun bindMainViewModel(viewModel: MainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SchedulesViewModel::class)
    internal abstract fun bindSchedulesViewModel(viewModel: SchedulesViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ScheduleDetailViewModel::class)
    internal abstract fun bindScheduleDetailViewModel(viewModel: ScheduleDetailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CreateContactViewModel::class)
    internal abstract fun bindCreateContactViewModel(viewModel: CreateContactViewModel): ViewModel

}