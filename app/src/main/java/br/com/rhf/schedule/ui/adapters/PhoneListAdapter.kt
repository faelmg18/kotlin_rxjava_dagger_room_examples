package br.com.rhf.schedule.ui.adapters

import android.app.Activity
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import br.com.rhf.domain.model.Phone
import br.com.rhf.schedule.R
import br.com.rhf.schedule.extensions.checkPhoneCallPermissionOrStartCallPhone
import br.com.rhf.schedule.extensions.inflate
import br.com.rhf.schedule.util.PhoneMask
import kotlinx.android.synthetic.main.phone_list_item.view.*

class PhoneListAdapter constructor(private val itemDeleteClick: (Phone) -> Unit, var context: Activity?) :
    ListAdapter<Phone, PhoneListAdapter.ViewHolder>(PhoneDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(parent)

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(getItem(position))

    inner class ViewHolder(parent: ViewGroup) :
        RecyclerView.ViewHolder(parent.inflate(R.layout.phone_list_item)) {

        fun bind(item: Phone) {
            itemView.textVIewPhone.text = item.phoneText
            itemView.phoneCall.setOnClickListener {
                context?.checkPhoneCallPermissionOrStartCallPhone(PhoneMask.replaceChars(item.phoneText))
            }
            itemView.phoneDelete.setOnClickListener {
                itemDeleteClick.invoke(item)
            }

            itemView.phoneType.setImageResource(getIconByPhoneType(item.phoneType))
        }
    }

    private class PhoneDiffCallback : DiffUtil.ItemCallback<Phone>() {
        override fun areItemsTheSame(oldItem: Phone, newItem: Phone): Boolean =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: Phone, newItem: Phone): Boolean =
            oldItem == newItem
    }

    private fun getIconByPhoneType(phoneType: String?): Int {

        val arrayListPhones = context?.resources?.getStringArray(R.array.phone_types_arrays)
        val homeLebal = context?.getString(R.string.home_label)
        val work = context?.getString(R.string.work_label)

        var icon: Int = R.drawable.ic_phone_android
        arrayListPhones?.forEach {

            if (phoneType == homeLebal && phoneType == it) {
                icon = R.drawable.ic_home
            } else if (phoneType == work && phoneType == it) {
                icon = R.drawable.ic_work
            }
        }

        return icon
    }
}
