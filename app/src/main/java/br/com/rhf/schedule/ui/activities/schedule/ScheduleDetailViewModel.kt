package br.com.rhf.schedule.ui.activities.schedule

import androidx.lifecycle.MutableLiveData
import br.com.rhf.data.database.repositories.PhoneRepository
import br.com.rhf.data.database.repositories.ScheduleRepository
import br.com.rhf.domain.interector.GetPhones
import br.com.rhf.domain.interector.GetSchedule
import br.com.rhf.domain.model.Phone
import br.com.rhf.domain.model.Schedule
import br.com.rhf.schedule.presentation.DefaultSingleObserver
import br.com.rhf.schedule.presentation.Resource
import br.com.rhf.schedule.ui.base.BaseViewModel
import javax.inject.Inject

class ScheduleDetailViewModel @Inject constructor(
    private val scheduleRepository: ScheduleRepository,
    private val phoneRepository: PhoneRepository,
    private val getPhones: GetPhones,
    private val getSchedule: GetSchedule
) :
    BaseViewModel() {

    val scheduleUpdateMutableLiveData = MutableLiveData<Boolean>()
    val scheduleDeleteMutableLiveData = MutableLiveData<Boolean>()
    val phoneDeleteMutableLiveData = MutableLiveData<Boolean>()
    val phoneInsertedMutableLiveData = MutableLiveData<Boolean>()
    val scheduleMutableLiveData = MutableLiveData<Resource<Schedule>>()
    val phonesMutableLiveData = MutableLiveData<Resource<MutableList<Phone>>>()


    override fun myOnCleared() {
    }

    fun getSchedule(id: Long) {
        getSchedule.executeSingle(object : DefaultSingleObserver<Schedule>(scheduleMutableLiveData) {}, id)
    }

    fun finAllPhoneByScheduleId(schedule: Schedule) {
        getPhones.executeSingle(object : DefaultSingleObserver<MutableList<Phone>>(phonesMutableLiveData) {}, schedule)
    }

    fun deletePhone(phone: Phone) {
        val deleted = phoneRepository.delete(phone) > 0
        phoneDeleteMutableLiveData.value = deleted
    }

    fun updateSchedule(schedule: Schedule) {
        var success = scheduleRepository.updateFavorite(schedule) > 0
        scheduleUpdateMutableLiveData.value = success
    }

    fun insertPhone(phone: Phone) {
        phoneRepository.savePhones(arrayListOf(phone))
        phoneInsertedMutableLiveData.value = true
    }

    fun deleteSchedule(schedule: Schedule) {
        var success = scheduleRepository.delete(schedule) > 0
        scheduleDeleteMutableLiveData.value = success
    }
}