package br.com.rhf.schedule.ui.activities.contacts

import androidx.lifecycle.MutableLiveData
import br.com.rhf.data.database.repositories.PhoneRepository
import br.com.rhf.data.database.repositories.ScheduleRepository
import br.com.rhf.domain.interector.GetSchedule
import br.com.rhf.domain.model.Contact
import br.com.rhf.domain.model.Schedule
import br.com.rhf.schedule.presentation.DefaultSingleObserver
import br.com.rhf.schedule.presentation.Resource
import br.com.rhf.schedule.ui.base.BaseViewModel
import javax.inject.Inject

class CreateContactViewModel @Inject constructor(
    var scheduleRepository: ScheduleRepository,
    var getSchedule: GetSchedule,
    var phoneRepository: PhoneRepository
) : BaseViewModel() {

    val contactMutableLiveData = MutableLiveData<Boolean>()
    val scheduleMutableLiveData = MutableLiveData<Resource<Schedule>>()

    fun saveContact(contact: Contact) {
        val id = scheduleRepository.insert(contact.schedule)

        (id > 0).let {
            if (it) {
                saveContact(contact, id)
            } else {
                contactMutableLiveData.value = false
            }
        }
    }

    private fun saveContact(contact: Contact, id: Long) {
        contact.phone.map {
            it.scheduleId = id
            return@map it
        }.let {
            phoneRepository.savePhones(it.toMutableList())
            contactMutableLiveData.value = true
        }
    }

    fun getScheduleById(scheduleId: Long) {
        getSchedule.executeSingle(object : DefaultSingleObserver<Schedule>(scheduleMutableLiveData) {}, scheduleId)
    }

    fun updateContact(schedule: Schedule) {
        scheduleRepository.update(schedule)
        contactMutableLiveData.value = true
    }

    override fun myOnCleared() {
        getSchedule.dispose()
    }
}