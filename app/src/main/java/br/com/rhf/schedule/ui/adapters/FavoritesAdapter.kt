package br.com.rhf.schedule.ui.adapters

import android.app.Activity
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import br.com.rhf.domain.model.Schedule
import br.com.rhf.schedule.R
import br.com.rhf.schedule.extensions.inflate
import br.com.rhf.schedule.extensions.loadImageRounded
import kotlinx.android.synthetic.main.favorites_item.view.*

class FavoritesAdapter constructor(
    private val itemClick: (Schedule) -> Unit,
    private val callClick: (Schedule) -> Unit,
    var context: Activity?
) :
    ListAdapter<Schedule, FavoritesAdapter.ViewHolder>(ScheduleDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(parent)

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(getItem(position))

    inner class ViewHolder(parent: ViewGroup) :
        RecyclerView.ViewHolder(parent.inflate(R.layout.favorites_item)) {

        fun bind(item: Schedule) {
            itemView.textViewInitialName.text = item.nome?.first().toString()
            itemView.textViewName.text = item.nome
            item.foto?.isNotEmpty().let {
                itemView.imageUser.loadImageRounded(item.foto)
            }
            itemView.setOnClickListener { itemClick.invoke(item) }

            itemView.call.setOnClickListener {
                callClick.invoke(item)
            }
        }
    }

    private class ScheduleDiffCallback : DiffUtil.ItemCallback<Schedule>() {
        override fun areItemsTheSame(oldItem: Schedule, newItem: Schedule): Boolean =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: Schedule, newItem: Schedule): Boolean =
            oldItem == newItem
    }
}
