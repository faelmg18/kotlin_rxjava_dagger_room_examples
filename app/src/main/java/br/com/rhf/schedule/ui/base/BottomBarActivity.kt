package br.com.rhf.schedule.ui.base

import android.os.Bundle
import androidx.fragment.app.Fragment
import br.com.rhf.schedule.R
import br.com.rhf.schedule.ui.fragments.FavoritesContactsFragment
import br.com.rhf.schedule.ui.fragments.SchedulesFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

abstract class BottomBarActivity<T : BaseViewModel> : BaseActivity<T>() {

    var currentFragment: Fragment? = null
        private set

    private var idItemSelected: Int = R.id.navigation_list_schedules

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->

        if (idItemSelected == item.itemId) {
            return@OnNavigationItemSelectedListener false
        }

        idItemSelected = item.itemId
        when (idItemSelected) {

            R.id.navigation_list_schedules -> {
                gotoFragment(SchedulesFragment::class.java, null)
                return@OnNavigationItemSelectedListener true
            }

            R.id.navigation_favorite_schedules -> {
                gotoFragment(FavoritesContactsFragment::class.java, null)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun myOnCreate(savedInstanceState: Bundle?) {
        navigationBar.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        gotoFragment(SchedulesFragment::class.java, null)
    }

    private fun updateNavigationBarState(actionId: Int) {
        val menu = navigationBar.menu

        var i = 0
        val size = menu.size()
        while (i < size) {
            val item = menu.getItem(i)
            item.isChecked = item.itemId == actionId
            i++
        }
    }

    protected fun gotoFragment(toFragment: Class<out Fragment>, bundle: Bundle?) {
        var fragment: Fragment? = null
        try {
            fragment = toFragment.newInstance()
            fragment!!.arguments = bundle

        } catch (e: InstantiationException) {
            e.printStackTrace()
        } catch (e: IllegalAccessException) {
            e.printStackTrace()
        }

        if (fragment == null)
            return

        currentFragment = fragment

        supportFragmentManager.beginTransaction()
            .replace(R.id.content_frame, fragment)
            .commit()
    }
}

