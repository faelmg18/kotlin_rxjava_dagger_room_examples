package br.com.rhf.schedule.ui.fragments

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import br.com.rhf.domain.model.Schedule
import br.com.rhf.schedule.MainActivity.Companion.SCHEDULE_ID_KEY
import br.com.rhf.schedule.MainActivity.Companion.UPDATE_SCHEDULES_REQUEST_CODE
import br.com.rhf.schedule.R
import br.com.rhf.schedule.extensions.showToast
import br.com.rhf.schedule.extensions.startRefreshing
import br.com.rhf.schedule.extensions.stopRefreshing
import br.com.rhf.schedule.presentation.Resource
import br.com.rhf.schedule.ui.activities.schedule.ScheduleDetailActivity
import br.com.rhf.schedule.ui.adapters.SchedulesAdapter
import br.com.rhf.schedule.ui.base.BaseFragment
import kotlinx.android.synthetic.main.list_schedules.*

class SchedulesFragment : BaseFragment<SchedulesViewModel>() {

    private val itemClick: (Schedule) -> Unit =
        {
            var bundle = Bundle()
            bundle.putLong(SCHEDULE_ID_KEY, it.id)
            gotoNextScreen(ScheduleDetailActivity::class.java, bundle, UPDATE_SCHEDULES_REQUEST_CODE)
        }
    private val adapter = SchedulesAdapter(itemClick)

    override fun layoutId(): Int {
        return R.layout.list_schedules
    }

    override fun onCreatedView() {
        mViewModel.scheduleMutableLiveData.observe(this, Observer { updateView(it) })
        scheduleRecyclerView.adapter = adapter
        swipeRefreshLayout.setOnRefreshListener { getSchedules() }
    }

    override fun onResume() {
        super.onResume()
        getSchedules()
    }

    private fun getSchedules() {
        mViewModel.getISchedule()
    }

    private fun updateView(resource: Resource<MutableList<Schedule>>?) {
        resource?.let {

            onStateChange(it)

            it.data.let { schedules ->
                adapter.submitList(it.data?.sortedBy { it.nome })
            }
            it.message?.let {
                mActivity?.showToast("error $it")
            }
        }
    }

    override fun onSuccess() {
        swipeRefreshLayout.stopRefreshing()
    }

    override fun onError() {
        swipeRefreshLayout.stopRefreshing()
    }

    override fun onLoading() {
        swipeRefreshLayout.startRefreshing()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == UPDATE_SCHEDULES_REQUEST_CODE) {
            getSchedules()
        }
    }
}