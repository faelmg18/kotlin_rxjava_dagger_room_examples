package br.com.rhf.data.network.request

import br.com.rhf.domain.model.Schedule
import io.reactivex.Single
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST

interface RestApi {
    @GET("users/")
    fun getUserToSchedule(): Single<MutableList<Schedule>>
}