package br.com.rhf.data.database.data

import androidx.room.*
import br.com.rhf.domain.model.Schedule
import io.reactivex.Maybe
import io.reactivex.Single

@Dao
abstract class ScheduleDao {

    @Query("SELECT * FROM Schedule WHERE id=:id")
    abstract fun findById(id: Long): Single<Schedule>

    @Query("SELECT * FROM Schedule")
    abstract fun findAll(): Single<MutableList<Schedule>>

    @Query("SELECT * FROM Schedule WHERE favorite=1")
    abstract fun findAllFavorites(): Single<MutableList<Schedule>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(schedule: Schedule): Long

    @Delete
    abstract fun delete(schedule: Schedule): Int

    @Query("UPDATE Schedule SET favorite =:favorite WHERE id=:id")
    abstract fun updateFavorite(id: Long, favorite: Boolean): Int

    @Update
    abstract fun update(schedule: Schedule)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    @JvmSuppressWildcards
    abstract fun insertAll(schedules: MutableList<Schedule>)

}
