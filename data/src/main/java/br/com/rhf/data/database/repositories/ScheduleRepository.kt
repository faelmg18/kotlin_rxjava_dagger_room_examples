package br.com.rhf.data.database.repositories

import br.com.rhf.domain.model.Schedule
import io.reactivex.Maybe
import io.reactivex.Single


interface ScheduleRepository {
    fun findById(id: Long): Single<Schedule>

    fun findAll(): Single<MutableList<Schedule>>

    fun findAllFavorites(): Single<MutableList<Schedule>>

    fun insert(schedule: Schedule): Long

    fun delete(schedule: Schedule): Int

    fun updateFavorite(schedule: Schedule): Int

    fun update(schedule: Schedule)

    fun saveAll(schedules: MutableList<Schedule>)
}