package br.com.rhf.data.modules

import br.com.rhf.data.network.request.CPFRestApi
import br.com.rhf.data.network.request.RestApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Named

@Module
class RequestModule {
    @Provides
    fun provideRequestApi(
        @Named("RetrofitApi") retrofit: Retrofit
    ): RestApi {
        return retrofit.create(RestApi::class.java)
    }

    @Provides
    fun provideRequestApiGerarCpf(
        @Named("RetrofitApiGerarCpf") retrofit: Retrofit
    ): CPFRestApi {
        return retrofit.create(CPFRestApi::class.java)
    }
}