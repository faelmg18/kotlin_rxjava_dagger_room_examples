package br.com.rhf.data.database.data

import androidx.room.*
import br.com.rhf.domain.model.Phone
import io.reactivex.Single

@Dao
abstract class PhoneDao {

    @Query("SELECT * FROM Phone WHERE scheduleId =:id")
    abstract fun findByScheduleId(id: Long): Single<MutableList<Phone>>

    @Query("SELECT * FROM Phone")
    abstract fun findAll(): Single<MutableList<Phone>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(phone: Phone): Long

    @Delete
    abstract fun delete(phone: Phone): Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    @JvmSuppressWildcards
    abstract fun insertAll(phone: MutableList<Phone>)

}
