package br.com.rhf.data.remote.repository

import br.com.rhf.data.database.repositories.PhoneRepository
import br.com.rhf.data.database.repositories.ScheduleRepository
import br.com.rhf.data.network.request.CPFRestApi
import br.com.rhf.data.network.request.RestApi
import br.com.rhf.domain.model.Phone
import br.com.rhf.domain.model.Schedule
import br.com.rhf.domain.repository.ISchendules
import io.reactivex.Single
import javax.inject.Inject

class ScheduleImpl @Inject constructor(
    private val api: RestApi,
    private val cpfRestApi: CPFRestApi,
    private val scheduleRepository: ScheduleRepository,
    private val phoneRepository: PhoneRepository
) :
    ISchendules {
    override fun findAllByScheduleId(schedule: Schedule): Single<MutableList<Phone>> {
        return phoneRepository.findAllByScheduleId(schedule)
    }

    override fun findAllFavorites(): Single<MutableList<Schedule>> {
        return scheduleRepository.findAllFavorites()
    }

    override fun update(schedule: Schedule): Int {
        return scheduleRepository.updateFavorite(schedule)
    }

    override fun saveAll(schedules: MutableList<Schedule>) {
        scheduleRepository.saveAll(schedules)
    }

    override fun findCpf(): Single<String> {
        return cpfRestApi.gerarCpf()
    }

    override fun findOnline(): Single<MutableList<Schedule>> {
        return api.getUserToSchedule()
    }

    override fun findById(id: Long): Single<Schedule> {
        return scheduleRepository.findById(id)
    }

    override fun findAll(): Single<MutableList<Schedule>> {
        return scheduleRepository.findAll()
    }

    override fun insert(schedule: Schedule): Long {
        return scheduleRepository.insert(schedule)
    }

    override fun delete(schedule: Schedule): Int {
        return scheduleRepository.delete(schedule)
    }

}