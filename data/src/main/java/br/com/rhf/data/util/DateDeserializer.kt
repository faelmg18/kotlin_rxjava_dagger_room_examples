package br.com.rhf.data.util

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonParseException
import java.lang.reflect.Type
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

internal class DateDeserializer : JsonDeserializer<Date> {

    @Throws(JsonParseException::class)
    override fun deserialize(
        jsonElement: JsonElement, typeOF: Type,
        context: JsonDeserializationContext
    ): Date {
        for (format in DATE_FORMATS) {
            try {
                if (jsonElement.asString.length >= 19) {
                    val date = jsonElement.asString.substring(0, 19)
                    return SimpleDateFormat(format).parse(date)
                }
                return SimpleDateFormat(format).parse(jsonElement.asString)
            } catch (e: IllegalArgumentException) {
                e.printStackTrace()
            } catch (e: ParseException) {
                e.printStackTrace()
            }

        }
        throw JsonParseException(
            "Unparseable date: \"" + jsonElement.asString
                    + "\". Supported formats: " +
                    Arrays.toString(DATE_FORMATS)
        )
    }

    companion object {

        private val DATE_FORMATS =
            arrayOf("yyyy-MM-dd'T'HH:mm:ss", "yyyy-MM-dd'T'HH:mm:ss'Z'", "yyyy-MM-dd'T'HH:mm:ss.SSS")
    }
}
