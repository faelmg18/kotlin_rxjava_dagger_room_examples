package br.com.rhf.data.modules

import android.app.Application
import androidx.room.Room
import br.com.rhf.data.database.ScheduleDatabase
import br.com.rhf.data.database.ScheduleDatabase.Companion.DATA_BASE_NAME
import br.com.rhf.data.database.data.PhoneDao
import br.com.rhf.data.database.data.ScheduleDao
import br.com.rhf.data.database.repositories.PhoneDatasource
import br.com.rhf.data.database.repositories.PhoneRepository
import br.com.rhf.data.database.repositories.ScheduleDatasource
import br.com.rhf.data.database.repositories.ScheduleRepository
import dagger.Module
import dagger.Provides
import javax.inject.Inject
import javax.inject.Singleton


@Module
class RoomModule @Inject constructor(app: Application) {

    private var scheduleDatabase: ScheduleDatabase =
        Room.databaseBuilder(app, ScheduleDatabase::class.java, DATA_BASE_NAME)
            .allowMainThreadQueries()
            .build()

    @Singleton
    @Provides
    fun providesRoomDatabase(): ScheduleDatabase {
        return scheduleDatabase
    }

    @Singleton
    @Provides
    fun providesScheduleDao(scheduleDatabase: ScheduleDatabase): ScheduleDao {
        return scheduleDatabase.scheduleDao
    }

    @Singleton
    @Provides
    fun providesPhoneDao(scheduleDatabase: ScheduleDatabase): PhoneDao {
        return scheduleDatabase.phoneDao
    }

    @Singleton
    @Provides
    fun scheduleRepository(scheduleDao: ScheduleDao): ScheduleRepository {
        return ScheduleDatasource(scheduleDao)
    }

    @Singleton
    @Provides
    fun phoneRepository(phoneDao: PhoneDao): PhoneRepository {
        return PhoneDatasource(phoneDao)
    }

}