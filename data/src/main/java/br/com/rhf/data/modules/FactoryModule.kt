package br.com.rhf.data.modules

import br.com.rhf.data.util.DateDeserializer
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import javax.inject.Singleton

@Module
class FactoryModule {

    @Provides
    @Singleton
    fun provideGsonFactory(): GsonConverterFactory {
        val builder = GsonBuilder().registerTypeAdapter(Date::class.java, DateDeserializer())
        return GsonConverterFactory.create(builder.create())
    }
}