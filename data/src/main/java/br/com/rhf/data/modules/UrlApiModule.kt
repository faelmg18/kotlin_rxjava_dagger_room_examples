package br.com.rhf.data.modules

import br.com.rhf.data.BuildConfig
import dagger.Module
import dagger.Provides
import javax.inject.Named

@Module
open class UrlApiModule {

    @Provides
    @Named("UrlApi")
    open fun provideUrlApi(): String {
        val host = BuildConfig.URL_USERS
        return "$host/"
    }

    @Provides
    @Named("UrlApiGerarCpf")
    open fun provideUrlApiGerarCpf(): String {
        val host = BuildConfig.CPF_URL
        return "$host/"
    }
}