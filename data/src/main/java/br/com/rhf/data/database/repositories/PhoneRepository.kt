package br.com.rhf.data.database.repositories

import br.com.rhf.domain.model.Phone
import br.com.rhf.domain.model.Schedule
import io.reactivex.Single

interface PhoneRepository {
    fun savePhones(phones: MutableList<Phone>)
    fun findAllByScheduleId(schedule: Schedule): Single<MutableList<Phone>>
    fun delete(phone: Phone): Int
}