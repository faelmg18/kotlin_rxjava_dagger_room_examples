package br.com.rhf.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import br.com.rhf.data.database.ScheduleDatabase.Companion.VERSION
import br.com.rhf.data.database.data.PhoneDao
import br.com.rhf.data.database.data.ScheduleDao
import br.com.rhf.domain.model.Phone
import br.com.rhf.domain.model.Schedule
import br.com.rhf.domain.util.PhoneConvert

@Database(entities = [Schedule::class, Phone::class], version = VERSION)
@TypeConverters(PhoneConvert::class)
abstract class ScheduleDatabase : RoomDatabase() {

    abstract val scheduleDao: ScheduleDao
    abstract val phoneDao: PhoneDao

    companion object {
        const val VERSION = 1
        const val DATA_BASE_NAME = "ScheduleDatabase-db"
    }
}
