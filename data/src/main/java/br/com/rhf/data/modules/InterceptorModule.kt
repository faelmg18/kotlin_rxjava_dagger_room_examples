package br.com.rhf.data.modules

import br.com.rhf.data.BuildConfig
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.logging.HttpLoggingInterceptor
import javax.inject.Named
import javax.inject.Singleton


@Module
class InterceptorModule {

    @Provides
    @Named("ResponseInterceptor")
    @Singleton
    fun provideInterceptorResponse(): Interceptor {
        return Interceptor { chain ->
            val requestBuilder = chain.request().newBuilder()
            requestBuilder.header("Content-Type", "application/json")
            chain.proceed(requestBuilder.build())
        }
    }

    @Provides
    @Singleton
    fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor.Level.BODY
        } else {
            HttpLoggingInterceptor.Level.NONE
        }

        return httpLoggingInterceptor
    }

    @Provides
    @Named("UserAgentInterceptor")
    @Singleton
    fun provideUserAgentInterceptor(): Interceptor {
        return Interceptor { chain ->
            val requestBuilder = chain.request().newBuilder()
            requestBuilder.addHeader("User-Agent", userAgent())
            chain.proceed(requestBuilder.build())
        }
    }

    private fun userAgent(): String {
        val version = BuildConfig.VERSION_CODE
        val osVersion = android.os.Build.VERSION.RELEASE
        val pack = "br.com.rhf.schedule"
        val device = android.os.Build.MODEL
        return "groupchallenge/$version ($pack; OS Version:$osVersion; Android $device)"
    }
}
