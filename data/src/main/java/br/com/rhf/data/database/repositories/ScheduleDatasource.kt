package br.com.rhf.data.database.repositories

import br.com.rhf.data.database.data.ScheduleDao
import br.com.rhf.domain.model.Schedule
import io.reactivex.Maybe
import io.reactivex.Single
import javax.inject.Inject

class ScheduleDatasource @Inject
constructor(private val scheduleDao: ScheduleDao) : ScheduleRepository {
    override fun update(schedule: Schedule) {
        scheduleDao.update(schedule)
    }

    override fun findAllFavorites(): Single<MutableList<Schedule>> {
        return scheduleDao.findAllFavorites()
    }

    override fun updateFavorite(schedule: Schedule): Int {
        return scheduleDao.updateFavorite(schedule.id, schedule.favorite)
    }

    override fun saveAll(schedules: MutableList<Schedule>) {
        scheduleDao.insertAll(schedules)
    }

    override fun findById(id: Long): Single<Schedule> {
        return scheduleDao.findById(id)
    }

    override fun findAll(): Single<MutableList<Schedule>> {
        return scheduleDao.findAll()
    }

    override fun insert(schedule: Schedule): Long {
        return scheduleDao.insert(schedule)
    }

    override fun delete(schedule: Schedule): Int {
        return scheduleDao.delete(schedule)
    }
}
