package br.com.rhf.data.database.repositories

import br.com.rhf.data.database.data.PhoneDao
import br.com.rhf.data.database.data.ScheduleDao
import br.com.rhf.domain.model.Phone
import br.com.rhf.domain.model.Schedule
import io.reactivex.Maybe
import io.reactivex.Single
import javax.inject.Inject

class PhoneDatasource @Inject
constructor(private val phoneDao: PhoneDao) : PhoneRepository {
    override fun delete(phone: Phone): Int {
        return phoneDao.delete(phone)
    }

    override fun savePhones(phones: MutableList<Phone>) {
        phoneDao.insertAll(phones)
    }

    override fun findAllByScheduleId(schedule: Schedule): Single<MutableList<Phone>> {
        return phoneDao.findByScheduleId(schedule.id)
    }
}
