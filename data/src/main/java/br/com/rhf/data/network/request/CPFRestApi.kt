package br.com.rhf.data.network.request

import io.reactivex.Single
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface CPFRestApi {
    @FormUrlEncoded
    @POST("ferramentas_online.php")
    fun gerarCpf(
        @Field("acao") token: String = "gerar_cpf"
    ): Single<String>

}