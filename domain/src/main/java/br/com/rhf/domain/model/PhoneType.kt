package br.com.rhf.domain.model

import androidx.room.TypeConverters
import br.com.rhf.domain.util.PhoneTypeConvertes
import com.google.gson.annotations.SerializedName

@TypeConverters(PhoneTypeConvertes::class)
enum class PhoneType constructor(var type: Int) {
    @SerializedName("0")
    CELULAR(0),
    @SerializedName("1")
    RESIDENCIAL(1),
    @SerializedName("2")
    TRABALHO(2)


}