package br.com.rhf.domain.interector

import br.com.rhf.domain.PostExecutionThread
import br.com.rhf.domain.executor.ThreadExecutor
import br.com.rhf.domain.model.Schedule
import br.com.rhf.domain.repository.ISchendules
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class GetFavoritesSchedules @Inject constructor(
    private val iSchedules: ISchendules,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) : UseCase<MutableList<Schedule>, Void?>(threadExecutor, postExecutionThread) {

    override fun buildUseCaseObservable(params: Void?): Observable<MutableList<Schedule>> {
        TODO("não se aplica") //To change body of created functions use File | Settings | File Templates.
    }

    override fun buildUseCaseSingle(params: Void?): Single<MutableList<Schedule>> {
        var schedule = iSchedules.findAllFavorites()
        return schedule

    }
}


