package br.com.rhf.domain.model

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class Phone(
    @SerializedName("telefone")
    val phoneText: String? = null,
    @SerializedName("tipoTelefone")
    val phoneType: String? = null,
    @PrimaryKey(autoGenerate = true)
    @SerializedName("id") var id: Long = 0,
    @SerializedName("scheduleId") var scheduleId: Long = 0
)
