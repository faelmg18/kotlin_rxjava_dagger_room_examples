package br.com.rhf.domain.util

import androidx.room.TypeConverter
import br.com.rhf.domain.model.PhoneType

object PhoneTypeConvertes {
    @TypeConverter
    fun fromStringToPhoneType(phoneType: Int): PhoneType {
        var phoneType1 = PhoneType.CELULAR
        when (phoneType) {
            1 -> phoneType1 = PhoneType.RESIDENCIAL
            2 -> phoneType1 = PhoneType.TRABALHO
        }
        return phoneType1
    }
}
