package br.com.rhf.domain.interector

import android.annotation.SuppressLint
import br.com.rhf.domain.PostExecutionThread
import br.com.rhf.domain.executor.ThreadExecutor
import br.com.rhf.domain.model.Phone
import br.com.rhf.domain.model.Schedule
import br.com.rhf.domain.repository.ISchendules
import io.reactivex.Observable
import io.reactivex.Single
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.xml.datatype.DatatypeConstants.SECONDS



class GetSchedules @Inject constructor(
    private val iSchedules: ISchendules,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) : UseCase<MutableList<Schedule>, Void?>(threadExecutor, postExecutionThread) {

    override fun buildUseCaseObservable(params: Void?): Observable<MutableList<Schedule>> {
        TODO("não se aplica") //To change body of created functions use File | Settings | File Templates.
    }

    override fun buildUseCaseSingle(params: Void?): Single<MutableList<Schedule>> {
        var schedule = iSchedules.findAll()

        return schedule.flatMap {
            if (it == null || it.size == 0) {
                return@flatMap findSchedulesOnline()
            } else {
                Single.just(it)
            }
        }
    }

    @SuppressLint("CheckResult")
    private fun findSchedulesOnline(): Single<MutableList<Schedule>> {


        return iSchedules.findOnline().flatMap { list ->
            val singleCpf = iSchedules.findCpf()
                //.retryWhen { errors -> errors.zipWith(Observable.range(1, 3).sinl, { _, i -> i }) }

            val map = list.map { schedule ->
                val cpf = singleCpf.blockingGet()
                schedule.CPF = cpf
                return@map schedule
            }

            if (map.isNotEmpty()) {
                iSchedules.saveAll(map.toMutableList())
            }

            return@flatMap Single.just(map.toMutableList())
        }
    }
}



