package br.com.rhf.domain.interector

import br.com.rhf.domain.PostExecutionThread
import br.com.rhf.domain.executor.ThreadExecutor
import br.com.rhf.domain.model.Phone
import br.com.rhf.domain.model.Schedule
import br.com.rhf.domain.repository.ISchendules
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class GetPhones @Inject constructor(
    private val iSchedules: ISchendules,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) : UseCase<MutableList<Phone>, Schedule>(threadExecutor, postExecutionThread) {
    override fun buildUseCaseObservable(params: Schedule): Observable<MutableList<Phone>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun buildUseCaseSingle(params: Schedule): Single<MutableList<Phone>> {
        return iSchedules.findAllByScheduleId(params)
    }
}


