package br.com.rhf.domain.executor

import java.util.concurrent.Executor

open interface ThreadExecutor : Executor