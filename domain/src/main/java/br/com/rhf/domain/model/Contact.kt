package br.com.rhf.domain.model

import com.google.gson.annotations.SerializedName

data class Contact(
    @SerializedName("schedule") var schedule: Schedule,
    @SerializedName("phone") var phone: MutableList<Phone>
)
