package br.com.rhf.domain.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity
data class Schedule(
    @PrimaryKey(autoGenerate = true)
    @SerializedName("id") var id: Long = 0,
    @SerializedName("name") var nome: String? = null,
    @SerializedName("sobrenome") var sobrenome: String? = null,
    @SerializedName("CPF") var CPF: String? = null,
    @SerializedName("email") var email: String? = null,
    @SerializedName("foto") var foto: String? = null,
    @SerializedName("favorite") var favorite: Boolean = false
) : Serializable {
}