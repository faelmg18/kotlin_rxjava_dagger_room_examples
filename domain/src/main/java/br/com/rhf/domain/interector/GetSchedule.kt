package br.com.rhf.domain.interector

import br.com.rhf.domain.PostExecutionThread
import br.com.rhf.domain.executor.ThreadExecutor
import br.com.rhf.domain.model.Schedule
import br.com.rhf.domain.repository.ISchendules
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class GetSchedule @Inject constructor(
    private val iSchedules: ISchendules,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) : UseCase<Schedule, Long>(threadExecutor, postExecutionThread) {

    override fun buildUseCaseObservable(params: Long): Observable<Schedule> {
        TODO("não se aplica") //To change body of created functions use File | Settings | File Templates.
    }

    override fun buildUseCaseSingle(id: Long): Single<Schedule> {
        var schedule = iSchedules.findById(id)
        return schedule
    }
}


