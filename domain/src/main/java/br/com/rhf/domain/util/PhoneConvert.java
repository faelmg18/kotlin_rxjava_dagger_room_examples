package br.com.rhf.domain.util;

import androidx.room.TypeConverter;
import br.com.rhf.domain.model.Phone;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class PhoneConvert {
    //room will automatically convert custom obj into string and store in DB
    @TypeConverter
    public static String
    convertMapArr(ArrayList<Phone> list) {
        Gson gson = new Gson();
        String json = gson.toJson(list);
        return json;
    }

    //At the time of fetching records room will automatically convert string to
    // respective obj
    @TypeConverter
    public static ArrayList<Phone>
    toMappedItem(String value) {
        Type listType = new
                TypeToken<ArrayList<Phone>>() {
                }.getType();
        return new Gson().fromJson(value, listType);
    }
}
