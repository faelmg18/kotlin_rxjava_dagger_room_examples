package br.com.rhf.domain.repository

import br.com.rhf.domain.model.Phone
import br.com.rhf.domain.model.Schedule
import io.reactivex.Single

interface ISchendules {

    fun findOnline(): Single<MutableList<Schedule>>

    fun findById(id: Long): Single<Schedule>

    fun findAll(): Single<MutableList<Schedule>>

    fun findAllFavorites(): Single<MutableList<Schedule>>

    fun insert(schedule: Schedule): Long

    fun saveAll(schedules: MutableList<Schedule>)

    fun delete(schedule: Schedule): Int

    fun update(schedule: Schedule): Int

    fun findAllByScheduleId(schedule: Schedule): Single<MutableList<Phone>>

    fun findCpf(): Single<String>
}